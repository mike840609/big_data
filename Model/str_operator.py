# coding=UTF-8
from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey

import re 

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import nltk


class str_operator:

    wordnet_lemmatizer = WordNetLemmatizer()
    stop = set(stopwords.words('english'))

    def __init__(self):
        # stop word list 
        nltk.download('punkt')
        nltk.download('stopwords')
        nltk.download("maxnet_treebank_pos_tagger")
        nltk.download("wordnet")
        

    def gen_keyword_list(self, title, doc_key , author_key):

        # set for unique element
        all_key = set(doc_key.split('; ') + author_key.split('; '))
        
        for key in all_key:
            if key in title:
                title = title.replace(key , '')
                
        # regular express                                
        title = re.sub('[^a-zA-Z]+', ' ', title)

        keyword = set(nltk.word_tokenize(title))
        # keyword += doc_key_list.split("; ")
        keyword = keyword.union(all_key)

        keyword = [x.lower() for x in keyword]

        filtered_words = [self.wordnet_lemmatizer.lemmatize(word) for word in keyword]
        filtered_words = [word for word in filtered_words if word not in self.stop]

        return filtered_words