# Journal Analysis

## Requirements
- python > 3.5

## Installation
- Create a virtual environment for a project: 
```basha
  
  $ cd desktop/Big_data/
  
  $ python -m venv venv

```

 ##### Virtualenv will isolate your Python/Django setup on a per-project basis.
- Start your virtual environment by running: 
```bash
    win :
    $ venv\Scripts\activate
    osx : 
    $ source \venv\bin\activate
```

- Installing requirements.txt:
```bash

    $ pip install -r requirements.txt

```

## Usage
- load pickle file 

```bash
    $ python lab_analytics.py

    $ self.paper_dict = pk.load(open(pickle_path, 'rb')) 
```