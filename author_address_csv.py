
from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey
import time
import operator
import os 
import pickle as pk
import glob
import json
import csv
import re

#  original data
apikey = "donforawandedledistincea"
password = "c835d0498d72f518bb7c806041830b74289f14f9"
url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
databaseName = "nest_test"


# apikey = "tionnedgedisonstrightfar"
# password = "4fa2bcedcdb281ee1f290e1d8fc081db0bf9111a"
# url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
# databaseName = "journal_db"


client = Cloudant(apikey, password, url=url, connect = True, auto_renew = True)
client.connect()
myDatabase = client[databaseName]
result_collection = Result(myDatabase.all_docs, include_docs=True)


author_address_arr = []
# format
# [[address_arr,author_arr] , .............]


# print(result_collection)
# performance improvement 
#for result in result_collection[0:100]:
for result in result_collection:
    try:
        data = result['doc']['C1']
        

        # print('{} + \n'.format(data))
        
        # find all authors in '[]' and aplit it into arr
        authors = re.findall(r"\[(.*?)\]",data)
        authors = [i.split('; ') for i in authors]

        # replace substring '[]' and split into arr
        address = re.sub(r"\[(.*?)\]", "", data)
        address = address.split('; ')
        
        
        
        
        for idx , auth in enumerate( authors):
            for au in auth:
                # print('{}:{}'.format(au ,address[idx] ))
                author_address_arr.append(tuple([au , address[idx]]))
    
    except KeyError:
        print("key not found")
    # print('=======================================')

print('result test ===============================')

# print(author_address_arr[0])
# print(author_address_arr[1])
# print(author_address_arr[2])

print('general list:{}'.format(len(author_address_arr)))
print('unique set :{}'.format(len(set(author_address_arr))))

path = os.path.join('CSV', 'author.csv')

if not os.path.exists('./CSV'):
    os.makedirs('./CSV')

with open(path,'w', newline='') as csvfile:
    #creating  a csv writer object
    csvwriter = csv.writer(csvfile)

    # writing the data rows
    csvwriter.writerows(sorted(set(author_address_arr)))
csvfile.close()
