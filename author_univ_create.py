# -*- coding: utf-8 -*- 
import csv
import os

author_info = []

f = open('CSV/author.csv', 'r')
for row in csv.reader(f):

    address = row[1]
    univ = address.split(',')[0]

    # author_arr.append(tuple(row))
    info = [row[0],univ , row[1]]
    author_info.append(info)

f.close()


if not os.path.exists('./CSV'):
    os.makedirs('./CSV')

path = os.path.join('CSV', 'author_univ.csv')

with open(path,'w', newline='') as csvfile:
    #creating  a csv writer object
    csvwriter = csv.writer(csvfile)

    # writing the data rows
    csvwriter.writerows(sorted(author_info))
    
csvfile.close()



