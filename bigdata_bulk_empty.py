from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey
import time
import operator
import os 
import pickle as pk
import json

# str operator 
from Model.str_operator import str_operator


apikey = "tionnedgedisonstrightfar"
password = "4fa2bcedcdb281ee1f290e1d8fc081db0bf9111a"
url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
databaseName = "journal_db"

# for test
# apikey = "busellessidenderemplyres"
# password = "b1d6726db9224a42dc7903310a8da13b6951e88e"
# url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
# databaseName = "journal_test"


mapping = {
    "ACADEMY OF MANAGEMENT JOURNAL": 4,
    "ACADEMY OF MANAGEMENT REVIEW": 4,
    "ACM TRANSACTIONS ON INFORMATION SYSTEMS": 1,
    "ADMINISTRATIVE SCIENCE QUARTERLY": 4,
    "COMMUNICATIONS OF THE ACM": 1,
    "DECISION SCIENCES": 3,
    "DECISION SUPPORT SYSTEMS": 1,
    "EUROPEAN JOURNAL OF INFORMATION SYSTEMS": 1,
    "IEEE TRANSACTIONS ON ENGINEERING MANAGEMENT": 3,
    "IEEE TRANSACTIONS ON KNOWLEDGE AND DATA ENGINEERING": 1,
    "IEEE TRANSACTIONS ON SOFTWARE ENGINEERING": 1,
    "IEEE-ACM TRANSACTIONS ON NETWORKING": 1,
    "INFORMATION & MANAGEMENT": 1,
    "INFORMATION SYSTEMS JOURNAL": 1,
    "INFORMATION SYSTEMS RESEARCH": 1,
    "INTERNATIONAL JOURNAL OF RESEARCH IN MARKETING": 2,
    "JOURNAL OF ADVERTISING": 2,
    "JOURNAL OF APPLIED PSYCHOLOGY": 4,
    "JOURNAL OF CONSUMER RESEARCH": 2,
    "JOURNAL OF INTERNATIONAL BUSINESS STUDIES": 4,
    "JOURNAL OF MANAGEMENT": 4,
    "JOURNAL OF MANAGEMENT INFORMATION SYSTEMS": 1,
    "JOURNAL OF MARKETING": 2,
    "JOURNAL OF MARKETING RESEARCH": 2,
    "JOURNAL OF OPERATIONS MANAGEMENT": 3,
    "JOURNAL OF RETAILING": 2,
    "JOURNAL OF THE ACADEMY OF MARKETING SCIENCE": 2,
    "MANAGEMENT SCIENCE": 3,
    "MARKETING SCIENCE": 2,
    "MIS QUARTERLY": 1,
    "OPERATIONS RESEARCH": 3,
    "ORGANIZATION SCIENCE": 4,
    "ORGANIZATIONAL BEHAVIOR AND HUMAN DECISION PROCESSES": 4,
    "PERSONNEL PSYCHOLOGY": 4,
    "PRODUCTION AND OPERATIONS MANAGEMENT": 3,
    "RESEARCH POLICY": 3,
    "STRATEGIC MANAGEMENT JOURNAL": 4
}

str_ope_obj = str_operator()

need_split_list = ['AU' , 'AF','DE', 'ID' , 'WC', 'SC' , 'C1']
new_field = ['EM' , 'FU']

def doSomething(myDocument):
    #Put your code here
    # print(json.dumps(myDocument, indent=3, sort_keys=True))

    # keyword
    # myDocument["keyword"] = str_ope_obj.gen_keyword_list(myDocument["TI"]  ,myDocument["ID"], myDocument["DE"]) 

    for k , v in myDocument.items():
        if k in new_field:
            str_v = str(v)
            if "; " in str_v :
                after_split = v.split("; ")
            else:
                after_split = [str(v)]
                
            myDocument[k] = after_split

        
    return myDocument

client = Cloudant(apikey, password, url=url, connect = True, auto_renew = True)
client.connect()
myDatabase = client[databaseName]

#Retrieve Docs from Cloudant
tStart = time.time()

results = dict()

print("load data from cloudant")
results = myDatabase.all_docs(include_docs=True)



tEnd = time.time()
print("Retrieval cost %f sec" % (tEnd - tStart))

# print(type(results))
# for data in results['rows']:
    # print(json.dumps(data, indent=3, sort_keys=True))
    # break

#Process

tStart = time.time()
docs = list()
big_docs = list()
sizes = dict()
count=0
for result in results['rows']:
    myDocument = result['doc']

    if 'SO' in myDocument and 'TI' in myDocument:
        count+=1

        #Call precess function
        myDocument = doSomething(myDocument)

        if len(myDocument['CR']) > 250:
            big_docs.append(myDocument)
        else:
            docs.append(myDocument)

tEnd = time.time()
print("Process cost %f sec" % (tEnd - tStart))
print("Total %d docs" % count)

# pk.dump(big_docs,  open('Pk_file/big_docs.p', "wb") ,protocol=4)
# pk.dump(docs,  open('Pk_file/docs.p', "wb") ,protocol=4)
# docs  =  pk.load(open('Pk_file/docs.p' , 'rb'))
# big_docs = pk.load(open('Pk_file/big_docs.p' , 'rb'))
# print(json.dumps(docs[1], indent=3, sort_keys=True))
# print(json.dumps(docs[2], indent=3, sort_keys=True))

print ('length docs:' + str(len(docs)))
print ('length big_data:' + str(len(big_docs)))

#Update Docs to Cloudant
print('start upload')

tStart = time.time()
i=0
while True:
    #print(i)
    if i+100 < len(docs):
        r = myDatabase.bulk_docs(docs[i:i+100])
        print(r)
    else:
        myDatabase.bulk_docs(docs[i:])
        break
    i+=100
j=100
while True:
    if j+5 < len(big_docs):
        myDatabase.bulk_docs(big_docs[j:j+5])
    else:
        myDatabase.bulk_docs(big_docs[j:])
        break
    j+=5
tEnd = time.time()
print("Update cost %f sec" % (tEnd - tStart))

client.disconnect()
