# a = [tuple(["mike", "test"]),tuple(["mike", "test"]),tuple(["mike", "test"])]
# print(set(a))

# -*- coding: utf-8 -*- 
import csv
import os

author_arr = []

# read
f = open('CSV/author.csv', 'r')
for row in csv.reader(f):
    author_arr.append(tuple(row))
f.close()

author_set = set(author_arr)

print(author_arr[0:5])
print('arr len: {}'.format(len(author_arr)))
print('set len: {}'.format(len(author_set)))



#  write
if not os.path.exists('./CSV'):
    os.makedirs('./CSV')

path = os.path.join('CSV', 'author_set.csv')

with open(path,'w', newline='') as csvfile:
    #creating  a csv writer object
    csvwriter = csv.writer(csvfile)

    # writing the data rows
    csvwriter.writerows(sorted(author_set))
    
csvfile.close()
