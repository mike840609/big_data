from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey

import json
import time
import pickle as pk

import os 


apikey = "tionnedgedisonstrightfar"
password = "4fa2bcedcdb281ee1f290e1d8fc081db0bf9111a"
url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
databaseName = "journal_db"


client = Cloudant(apikey, password, url=url, connect = True, auto_renew = True)
client.connect()
myDatabase = client[databaseName]


result_collection = Result(myDatabase.all_docs, include_docs=True)

data_arr = []

tStart = time.time()

for data in result_collection:
    try:
        data_arr.append(data['doc'])

    except KeyError:
        print("key not found")

tEnd = time.time()
print("Process cost %f sec" % (tEnd - tStart))
print("Total %d docs" % len(data_arr))

folder = 'Data_Clone'

if not os.path.exists('./Data_Clone'):
    os.makedirs('./Data_Clone')


# clone data and split to multiple file =============================
data_cache = []
count = 0
for idx,doc in enumerate(data_arr):

    data_cache.append(doc)
    
    if (idx+1) % 1000 == 0 :    
        count += 1
        pk.dump(data_cache,  open('Data_Clone/clone_data_{}.p'.format(count), "wb"))
        data_cache = []

count += 1
pk.dump(data_cache,  open('Data_Clone/clone_data_{}.p'.format(count), "wb"))



#  reader =========================================================
data_pk = []

for f in os.listdir(folder):
    path = os.path.join(folder , f)
    data_pk += (pk.load(open(path, 'rb')))

# test ====================================

print(len(data_arr))
print(len(data_pk))

data_arr = sorted(data_arr,key=lambda k: k['_id'])
data_pk = sorted(data_pk,key=lambda k: k['_id'])


print(data_arr[0])
print(data_pk[0])
print('================================================')
print(data_arr[-1])
print(data_pk[-1])

