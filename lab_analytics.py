# coding=UTF-8
import json
import base64

import os
import glob
from os import listdir
from os.path import isfile, join
import pickle as pk

from Model.str_operator import str_operator
import nltk

# why dict :
# Lookups in lists are O(n), lookups in dictionaries are amortized O(1),
# with regard to the number of items in the data structure. If you don't need to associate values, use sets.

# handle pickle file size limit
# n_bytes = 2**31
# max_bytes = 2**31 - 1



class file_operator():

    domain_name = ''

    # txt file path
    docs_dict = dict()

    # paper dictionary
    paper_dict = dict()

    str_ope_obj = str_operator()

    # template key
    counter = 0

    mypath = ''

    def __init__(self, path):
        self.mypath = path
        self.domain_name = os.path.basename(path)
        # print(self.domain_name)

    # private func ========================================================
    # generate file path for all txt file
    def __gen_file_dict(self):

        all_folder_dict = glob.glob(os.path.join(self.mypath, '*'))
        
        for folder_name in all_folder_dict:
            text_files = [f for f in os.listdir(folder_name) if (f.endswith('.txt'))]
            self.docs_dict[folder_name] = text_files
        # print(json.dumps(self.docs_dict, indent=3, sort_keys=True))

    #  generate .p picke file
    def __gen_pickle_dict(self):

        for journal_name, j_list in self.docs_dict.items():
            for part in j_list:

                # filepath = journal_name + '/' + part
                filepath = os.path.join( journal_name, part)

                with open(filepath, 'rb') as content:
                    col_title = list()
                    lines = content.read().decode("utf-16-le").split('\n')

                    for col in lines[0].split('\t'):
                        col_title.append(col)

                    # special character filter
                    col_title[0] = col_title[0][-2:]
                    col_title[-1] = col_title[-1][:2]

                    for line in lines[1:-1]:
                        self.counter += 1

                        data = dict()
                        cols = line.split('\t')

                        for i in range(len(col_title)):
                            # ; split condition
                            if '; ' in cols[i] and col_title[i] != 'TI' and col_title[i] != 'ID' and col_title[i] != 'DE':
                                after_split = cols[i].split("; ")
                                data[col_title[i]] = after_split
                                # print(cites)

                            else:
                                data[col_title[i]] = cols[i]
                        
                        # data["keyword"] = self.str_ope_obj.gen_keyword_list(data["TI"]  ,data["ID"])
                        data["keyword"] = self.str_ope_obj.gen_keyword_list(data["TI"]  ,data["ID"], data["DE"]) 
                        self.paper_dict[self.counter] = data

        
        pickle_path = os.path.join('Pk_file', self.domain_name+'.p')
        
        f = open(pickle_path, "wb")
        pk.dump(self.paper_dict,  f ,protocol=4)
        f.close()
         
        # p = pk.Pickler(open(pickle_path,"wb") , pk.HIGHEST_PROTOCOL)
        # p.fast = True
        # p.dump(self.paper_dict)
        # p.clear_memo()
        
        


    # public func =======================================================================
    def get_file_dict(self):
        self.__gen_file_dict()
        return self.docs_dict

    def get_paper_dict(self):
        
        pickle_path = os.path.join('Pk_file', self.domain_name+'.p')

        if os.path.exists(pickle_path):
            # exist -> load file
            self.paper_dict = pk.load(open(pickle_path, 'rb'))
            # self.__gen_file_dict()
            # self.__gen_pickle_dict()

        else:
            # not exist -> generate pickle file
            self.__gen_file_dict()
            self.__gen_pickle_dict()

        return self.paper_dict



# =================================================================================================
import nltk
def main():
    
    # mypath = "./Journal_Paper_Source"
    mypath = "./Journal_Paper_Source"
    sub_folder_dict = glob.glob(os.path.join(mypath, '*'))
    
    for domain in sub_folder_dict:
        ope_obj = file_operator(domain)
        paper_dict = ope_obj.get_paper_dict()
        print('{} \npaper_num:{}\n'.format( os.path.basename(domain) ,len(paper_dict)))
        # print(json.dumps(paper_dict[1], indent=3, sort_keys=True))

    '''
    ope_obj = file_operator(mypath)
    paper_dict = ope_obj.get_paper_dict()
    print(len(paper_dict))
    print(json.dumps(paper_dict[1], indent=3, sort_keys=True))
    
    key_freq_dict = dict()
    '''

    # for k , v in paper_dict.items():
        # for key in v["keyword"]:
            # print (key)
        # print("===")

    # pandas opration 
    # docs_path = ope_obj.get_file_dict()
    # print(json.dumps(docs_path, indent=3, sort_keys=True))

    # pandas_test(docs_path)



# test function ====================================================================================
import pandas as pd
def pandas_test(file_dict):

    files_list = []

    for journal_name, j_list in file_dict.items():
        for part in j_list:
            file_path = journal_name + '/' + part
            files_list.append(file_path)
    
    #  concatenate all file dataframe into one DataFrame
    df = pd.concat((pd.read_csv(f,sep='\t',index_col=False,encoding='utf-16-le') for f in files_list))

    # time column
    df["time"] = df['PD'].astype(str) + df['PY'].astype(str)

    # select column in dataframe
    df1 = df[['AU','DI','time']]

    # print(df.dtypes)
    # print(df.head(3))
    # print(df.tail(3))

    # print(df1.head(3))
    # print(df1.shape)

    print (df1)
    



    #  index_file = flase fix read issue
    '''
    df = pd.read_csv(test_file_path,
                       sep='\t',
                       index_col=False,
                       encoding='utf-16-le')
    '''

    # first row record
    # print(df.iloc[0])
    # print(df.AU)
    # print(df.shape)
    

if __name__ == '__main__':
    main()

    
