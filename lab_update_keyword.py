from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import nltk

apikey = "toneduartypilleadelfphic"
password = "ca814b577b9661d12e66d7395d62b3c0048e45ce"
url = "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
databaseName = "nest_test"


client = Cloudant(apikey, password, url=url, connect = True, auto_renew = True)
client.connect()
myDatabase = client[databaseName]

#if myDatabase.exists():
#    print("'{0}' successfully connected.\n".format(databaseName))

nltk.download('stopwords')
nltk.download("maxnet_treebank_pos_tagger")
nltk.download("wordnet")
nltk.download('punkt')

stop = set(stopwords.words('english'))
wordnet_lemmatizer = WordNetLemmatizer()

result_collection = Result(myDatabase.all_docs)
count = 0


new_doc_list = []

for result in result_collection:

    myDocument = myDatabase[result['id']]
    docTitle = myDocument['TI']
    docKeyword = myDocument['ID'].split("; ")

    keyword = nltk.word_tokenize(docTitle)
    keyword += docKeyword

    keyword = [x.lower() for x in keyword]
    
    filtered_words = [wordnet_lemmatizer.lemmatize(word) for word in keyword]
    filtered_words = [word for word in filtered_words if word not in stop]


    myDocument["keyword"] = filtered_words
    count += 1 
    print(str(count) + "uplopad done")
    myDocument.save()
    
client.disconnect()
