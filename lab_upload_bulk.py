# coding=UTF-8

import json
import requests
import base64

import os
import glob
from os import listdir
from os.path import isfile, join
import re

from Model.str_operator import str_operator



credential = {
  "username": "c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix",
  "password": "3834975540306ccacb87e5c7f311a04b49ff9ab184dda3faa770d81fd2f30c0f",
  "host": "c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com",
  "port": 443,
  "url": "https://c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix:3834975540306ccacb87e5c7f311a04b49ff9ab184dda3faa770d81fd2f30c0f@c2b88cf9-c42f-49bf-82ea-4c981dbc3692-bluemix.cloudant.com"
}

string = credential["username"]+":"+credential["password"]
auth = base64.b64encode(string.encode('utf-8')).decode('utf-8')
header = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic ' + auth
}



'''
post to database 
'''
count = 0 
str_ope_obj = str_operator()

def post_to_server2(path):
    # global count

    count = 0 

    data_arr = []
    with open(path, 'rb') as content:
        col_title = list()
        lines = content.read().decode("utf-16-le").split('\n')
        #print(content.read().decode("utf-8"))
        for col in lines[0].split('\t'):
            col_title.append(col)
        col_title[0] = col_title[0][-2:]
        col_title[-1] = col_title[-1][:2]
        #print(col_title)


        for line in lines[1:-1]:

            count += 1
            
            data = dict()
            
            cols = line.split('\t')

            for i in range(len(col_title)):
                # CR condition
                if col_title[i] == 'CR':
                    cites  = cols[i].split(";") 
                    print(cites)
                    data[col_title[i]] = cites
                else:
                    data[col_title[i]] = cols[i]
            
            # keyword list
            data["keyword"] = str_ope_obj.gen_keyword_list(data["TI"] , data["ID"])

            print (data["keyword"])
            data_arr.append(data)

            # 100 , 200 ,300 ,
            if count % 25 == 0 :

                count = 0
                r = requests.post(credential["url"]+'/nest_test/_bulk_docs', headers = header, data=json.dumps({'docs':data_arr}))
                print(r.text)
                data_arr = []
                

        
        # tial items < 100
        r = requests.post(credential["url"]+'/nest_test/_bulk_docs', headers = header, data=json.dumps({'docs':data_arr}))



# =================================================================================================

'''
create all file path 
'''
mypath = "./Journal_Paper_Source"
all_folder_dict = glob.glob(os.path.join(mypath, '*'))
docs_dict = dict()

#  read all file name to file_list array 
for folder_name in all_folder_dict:
    text_files = [f for f in os.listdir(folder_name) if (f.endswith('.txt'))]
    docs_dict[folder_name] =  text_files
# print(json.dumps(docs_dict, indent=3, sort_keys=True))

for journal_name,j_list in docs_dict.items():
    for part in j_list:
        filepath = journal_name + '/'+ part
        print(filepath + ("========================================="))
        post_to_server2(filepath)
    
    

    

