aiodns==1.0.0
bencodepy==0.9.5
blessings==1.6
certifi==2018.1.18
chardet==3.0.4
cloudant==2.7.0
cycler==0.10.0
idna==2.6
ih2torrent==0.1.14
matplotlib==2.1.2
nltk==3.2.5
numpy==1.14.0
nvidia-ml-py3==7.352.0
pandas==0.22.0
psutil==5.4.1
pycares==2.3.0
pyparsing==2.2.0
python-dateutil==2.6.1
pytz==2017.3
requests==2.18.4
six==1.11.0
urllib3==1.22
